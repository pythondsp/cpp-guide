// forward_list_stl_ex.cpp 

#include <iostream>
#include <forward_list>

using namespace std;

int main(){
    forward_list<int> lst;
    
    for (int i=0; i<5; i++)
        // lst.push_back(i*2); // push_back will not work
        lst.push_front(i*2);

    cout << "Print using for loop" << endl;
    for (int l : lst) // for l in list:
        cout << l << " "; // print(l)
    cout << endl;

    cout << "Print using while loop" << endl;
    while (! lst.empty()){
        cout << lst.front() << " "; // print first element
        lst.pop_front(); // remove the first element
    }
    cout << endl;

    // // index and random access is not allowed in list
    // for (int i=0; i<lst.size(); i++)
        // cout << lst[i];

    return 0;
}

