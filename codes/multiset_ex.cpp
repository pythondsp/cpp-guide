// multiset_ex.cpp

#include <iostream>
#include <string>
#include <set>

using namespace std;

int main(){
    set<string> names { "Meher", "Krishna", "Patel", "Meher"};
    multiset<string> names_mul { "Meher", "Krishna", "Patel", "Meher"};
    
    names.insert("mekrip"); // insert new name

    cout << "set" << endl; // set
    for (string name : names)
        cout << name << " ";
    cout << endl << endl;

    cout << "multiset" << endl; // multiset
    for (string name : names_mul)
        cout << name << " ";
    cout << endl;

    return 0;
}
