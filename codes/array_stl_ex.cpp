// array_stl_ex.cpp

#include <iostream>
#include <array>

using namespace std;

int main(){
    array<int, 5> arr = {3, 4}; // int-array of size 5

    // print the values
    for (int i=0; i<arr.size(); i++)
        cout << i << ": " << arr[i] << endl;

    return 0;
}
    
