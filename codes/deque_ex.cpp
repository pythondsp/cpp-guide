// deque_ex.cpp

# include <iostream>
#include <deque>

using namespace std; 

int main(){
    deque<int> deq = {1, 2}; // initialized deque
    cout << "size: " << deq.size() << endl;
    
    deq.push_front(10); // insert at the front
    deq.push_back(20); // insert in the end

    for (int i=0; i<deq.size(); i++)
        cout << deq[i] << ", ";

    cout << endl;

    return 0;
}
