// vector_ex.cpp

#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

int main(){
    vector<double> vec;

    // append elements
    for (int i=0; i<4; i++)
        vec.push_back(sqrt(i)); // sqrt(i)

    // print elements
    for (int i=0; i<vec.size(); i++)
       cout << vec[i] << endl;

   return 0;
} 
    



    
