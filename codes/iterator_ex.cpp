// iterator_ex.cpp

#include <iostream>
#include <vector>

using namespace std;

int main() {
    vector<int> v_int; // container : vector of type 'int'
    int i = 0;

    for (int i=0; i<5; i++) // int i : local 'i' 
        v_int.push_back(i); 

    // iterator
    vector<int>::iterator vi;

    for (vi = v_int.begin(); vi != v_int.end(); ++vi)
       cout << *vi << " "; 

    cout << endl;

    return 0;
}
