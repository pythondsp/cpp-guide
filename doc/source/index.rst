.. Programming with C and C++ documentation master file, created by
   sphinx-quickstart on Sat Dec 30 05:02:03 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Programming with C and C++
==========================

.. toctree::
    :numbered:
    :maxdepth: 3
    :caption: Contents:

    cpp/basic
    cpp/datatype
    cpp/control
    cpp/function
    cpp/array
    cpp/structure
    cpp/fileio
    cpp/multifile
    cpp/formatIO
    cpp/moreex
    cpp/cppintro
    cpp/fileiocpp
    cpp/stl
    cpp/oops
    cpp/exception
