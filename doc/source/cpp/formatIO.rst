.. _`ch_FormatIO`:

Formatted Inputs and Outputs
****************************

.. raw:: latex

    \chapterquote{The only way of not being upset by the blame is to be detached from the praise also; it is only through complete detachment that a person  can keep unmoved by the opposites of praise and blame.}{Meher Baba}


Introduction
============

In :numref:`c_arrayEx`, we used '\%4d' and '\%21.5' etc. to print the outputs in readable format. In this chapter, we will some more commands for formatting input and outputs i.e. 'scanf' and 'printf' commands respectively. 

Printing Integer and Floating point values
==========================================

:numref:`tbl_FormatIntFloat` shows the list of 'conversion specifier' e.g. 'd' and 'f' etc. for printing 'Integer' and 'Floating point' values. Further, these specifiers are used to print the values in different formats in :numref:`c_printIntFloat`. 


.. _`tbl_FormatIntFloat`:

.. table:: Print numbers in various formats
        
    +---------------------------+---------------------------------------------+
    | **Integer Format**        | **Description**                             |
    +===========================+=============================================+
    | d or i                    | Signed decimal integer                      |
    +---------------------------+---------------------------------------------+
    | o                         | Unsigned Octal integer                      |
    +---------------------------+---------------------------------------------+
    | u                         | Unsigned decimal integer                    |
    +---------------------------+---------------------------------------------+
    | x or X                    | Unsigned Hexadecimal integer                |
    +---------------------------+---------------------------------------------+
    | **Floating Point Format** | **Description**                             |
    +---------------------------+---------------------------------------------+
    | f                         | Fixed notation (e.g. 102.300003)            |
    +---------------------------+---------------------------------------------+
    | e or E                    | Exponential notation (e.g. 1.023000e+002)   |
    +---------------------------+---------------------------------------------+
    | g or G                    | Display in format 'f' or 'e' based on value |
    +---------------------------+---------------------------------------------+


.. literalinclude:: codes/Formatted-Inputs-and-Outputs/printIntFloat.c
    :language: c++
    :linenos:
    :caption: Different formats Integer and Float values
    :name: c_printIntFloat


Modify 'printf' format
======================

:numref:`tbl_printfFormat` shows various flags which can be used to format the outputs for the 'printf' statement. :numref:`c_printfFlag` and :numref:`c_printfPrefix` show the usage of these flags. Please read the comments under these listings. 


.. _`tbl_printfFormat`:

.. table:: 'printf' formats

    +-------+-----------------------------------------------------------+
    | Flag  | Description                                               |
    +=======+===========================================================+
    | \+    | Right justified                                           |
    +-------+-----------------------------------------------------------+
    | \-    | Left justified                                            |
    +-------+-----------------------------------------------------------+
    | #     | Prefix 'o' and 'x' for octal and hex outputs respectively |
    +-------+-----------------------------------------------------------+
    | space | Prints space before positive values                       |
    +-------+-----------------------------------------------------------+
    | 0     | Pad leading zeros                                         |
    +-------+-----------------------------------------------------------+



.. literalinclude:: codes/Formatted-Inputs-and-Outputs/printfFlag.c
    :language: c++
    :linenos:
    :caption: 'Left \& right justified' and 'signs'
    :name: c_printfFlag


.. literalinclude:: codes/Formatted-Inputs-and-Outputs/printfPrefix.c
    :language: c++
    :linenos:
    :caption: '\#' to print '0x' and '0' before hex and oct numbers respectively
    :name: c_printfPrefix


Formatted input using 'Scanf'
=============================

In this section, we will see various methods to get inputs from user with 'scanf' command. 

Format specifiers
-----------------

We can use all the formats specified in :numref:`tbl_FormatIntFloat` with 'scanf' as well. The difference is in 'd' and 'i' specifiers i.e. 'd' can only read integer format, where 'i' can read 'Octal' and 'Hexadecimal' formats as well, as shown in :numref:`c_scanfExFormat`.

.. literalinclude:: codes/Formatted-Inputs-and-Outputs/scanfEx.c
    :language: c++
    :linenos:
    :caption: Difference in 'd' and 'i'
    :name: c_scanfExFormat

Scan and rejection set
----------------------

We can select or reject data from the input provided by user, as shown in :numref:`c_scanSetEx` and :numref:`c_invertedScanSetEx` respectively. The '[ abc ]' is used for creating the scan-set, whereas :math:`\hat abc` is used for rejection-set. Please see comments in the listing for better understanding. 

.. literalinclude:: codes/Formatted-Inputs-and-Outputs/scanSetEx.c
    :language: c++
    :linenos:
    :caption: Scan set
    :name: c_scanSetEx

.. literalinclude:: codes/Formatted-Inputs-and-Outputs/invertedScanSetEx.c
    :language: c++
    :linenos:
    :caption: Inverted scan set (reject set)
    :name: c_invertedScanSetEx

 
Suppression character '\*'
--------------------------

Suppression character can be used for removing certain characters while reading input e.g. while reading date in 'dd-mm-yyyy', we can skip the '-' as shown in :numref:`c_suppressCharEx`, where two methods are shown for suppressing the characters. 

.. literalinclude:: codes/Formatted-Inputs-and-Outputs/suppressCharEx.c
    :language: c++
    :linenos:
    :caption: Suppression character
    :name: c_suppressCharEx


Conclusion
==========
    
In this section, we saw various methods to format the input and output data using 'scanf' and 'printf' command respectively. 