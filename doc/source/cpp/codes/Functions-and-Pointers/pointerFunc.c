// pointerFunc.c

#include <stdio.h>

// pointer to function
// cube and square have save input and return type. 
// function point is defined based on these types as below,
// function pointer: typedef return_type (*funcName)(input_type)
typedef int (*operation)(int);

// return cube value
int cube(int i){
    return i*i*i;
}

// return square value
int square(int i){
    return i*i;
}

// perform_operation based on 'op' value
int perform_operation(int i, operation op){
    return op(i);
}

int main(){

    int a = 2;
    int b; 

    b = perform_operation(a, cube); // cube 
    printf("cube of %d = %d\n", a, b);

    b = perform_operation(a, square); // square 
    printf("square of %d = %d\n", a, b);

    return 0;
}


/* Outputs
cube of 2  =  8
square of 2  =  4
*/
