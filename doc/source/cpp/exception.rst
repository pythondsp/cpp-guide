.. _`ch_ExceptionHandaling`:

Exception handling
******************

.. raw:: latex

    \chapterquote{When a man is, through his desires, confronted with great suffering , he understands their true nature; so when such suffering comes, it should be welcome. Suffering may come in order to eliminate further suffering. Suffering has to come, when it is of use in purging the soul from it's desires. }{Meher Baba}


Introduction
============

In this chapter, examples of exceptional handling are shown. 



Type checking
=============

.. literalinclude:: codes/Exception-handling/typeChecking.cpp
    :language: c++
    :linenos:
    :caption: ype checking
    :name: cpp_typeChecking

.. literalinclude:: codes/Exception-handling/typeChecking2.cpp
    :language: c++
    :linenos:
    :caption: Type checking
    :name: cpp_typeChecking2


Exceptional handling
====================


.. literalinclude:: codes/Exception-handling/exceptionEx.cpp
    :language: c++
    :linenos:
    :caption: Exceptional handling
    :name: cpp_exceptionEx

.. literalinclude:: codes/Exception-handling/exceptionEx2.cpp
    :language: c++
    :linenos:
    :caption: Exceptional handling
    :name: cpp_exceptionEx2

