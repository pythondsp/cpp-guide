.. _`ch_basicEx`:

More Examples
*************

.. raw:: latex

    \chapterquote{When a man is, through his desires, confronted with great suffering , he understands their true nature; so when such suffering comes, it should be welcome. Suffering may come in order to eliminate further suffering. Suffering has to come, when it is of use in purging the soul from it's desires. }{Meher Baba}

Introduction
============

In previous chapters, we saw various useful features of C language with some examples. In this chapter, more examples are added for better understanding of the language. Only new and/or important parts of the codes are discussed in this chapter. 

Basic Examples
==============

In this section, some simple examples are shown to learn the language effectively.

Print number in reverse order
-----------------------------

The 'for loop' can be executed in reverse direction using 'i\-\-' operator as shown at Line 8 of :numref:`c_reverseOrder`. 

.. literalinclude:: codes/More-Examples/reverseOrder.c
    :language: c++
    :linenos:
    :caption: Print number in reverse order
    :name: c_reverseOrder


Factorial
---------

:numref:`c_factorialEx` calculates the factorial of numbers. In this listing, 'factorial \*= i' is the sort notation for 'factorial = factorial \* i'. Similarly, we can use 'a += 3' for 'a = a + 3' etc. 


.. literalinclude:: codes/More-Examples/factorialEx.c
    :language: c++
    :linenos:
    :caption: Factorial
    :name: c_factorialEx


Fibonacci series
----------------

:numref:`c_Fibonacci` prints the Fibonacci series. In this listing, 'infinite for loop' is used at Line 15, which is terminated at Line 24, when the next Fibonacci number is greater than the 'limit'  provided by the user at Lines 10-11.  


.. literalinclude:: codes/More-Examples/Fibonacci.c
    :language: c++
    :linenos:
    :caption: Fibonacci series
    :name: c_Fibonacci


.. _`value_EOF_ENTER`:

Print values of characters e.g. EOF, Space and Enter etc.
---------------------------------------------------------

'Spaces', 'EOF (End of file)', 'Commas' and 'Enter (i.e. line-change) can be considered as the end of data e.g. in CSV file, the comma is the delimiter which distinguish one data from other. Therefore, we need to identity these values while working with data. Further, The symbolic constant 'EOF (End of file)' is an integer and defined in 'stdio.h'. The '**getchar**' command returns different values for different characters. 

**Explanation** :numref:`c_EOF_ex`

    In the listing, Line 8 prints the value 'EOF' i.e. -1. Next , 'getchar' command is used (Line 14) to read the values from terminal and then corresponding 'integer' values are printed using 'printf' command. Here, infinite-while-loop is used  to read the values from the terminal (Line 13) and 'E (uppercase E)' is used to break the loop.  **Note that, 'int c' is used (not 'char c'), to store the larger values and 'EOF', which is not possible with 'char c'**. In the outputs, we can see the different values for different characters; some of those values are listed below, 

    * t = 116
    * space = 32
    * enter = 10
    * EOF (i.e. ctrl+z) = -1 



.. literalinclude:: codes/More-Examples/EOF_ex.c
    :language: c++
    :linenos:
    :caption: Print values of characters e.g. EOF, Space and Enter etc.
    :name: c_EOF_ex



Read and write until EOF
------------------------

In :numref:`value_EOF_ENTER`, we saw that 'EOF' is the constant, which is stored in the 'stdio.h' header file; and infinite loop was terminated with the help of character 'E' in 'if loop'. Now, In :numref:`c_getPutEOF`, we will use the 'EOF' to terminate the program. Also, '**putchar**' command is used (Line 13) to print the character, instead of 'printf'. 


.. literalinclude:: codes/More-Examples/getPutEOF.c
    :language: c++
    :linenos:
    :caption: read and write until EOF
    :name: c_getPutEOF

Counting characters
-------------------

:numref:`c_countChar`, counts the total number of characters entered before EOF operation. Here, 'getchar()' is used inside the 'while loop' i.e. character will be read from the terminal until the compiler gets the 'ctrl+Z' command; also, we do no not need 'char c' for storing the character, as we did in :numref:`c_getPutEOF`. 


.. literalinclude:: codes/More-Examples/countChar.c
    :language: c++
    :linenos:
    :caption: Counting characters
    :name: c_countChar


Counting spaces, words and lines
--------------------------------

:numref:`c_countLine` counts the spaces, words and spaces. Note that, single quotes (not double) are used to define space i.e. ' '. Also, word counts are based on space and enters, therefore 'word\_count' is used in both the 'if statements' i.e. Lines 13 and 18. 


.. literalinclude:: codes/More-Examples/countLine.c
    :language: c++
    :linenos:
    :caption: Counting characters
    :name: c_countLine


Print longest line
------------------

:numref:`c_largest_line` prints the longest line entered by the user. Here, function 'getLine' (Line 8), checks the length of the line; which is called by 'main' function through Line 37'. Note that, the condition for 'while loop' at Line 38 is '\> 0'; since the 'EOF' command returns '0' therefore, the loop will be terminated by 'ctrl+z' command. Also, automatic-variable 'k' (Line 46) is local to 'if statement' at Line 39 (see comments for further details). Lastly,  'MAX\_CHAR - 2' is used at Line 13, to store the 'NULL character i.e. \\ 0' at the end of the string. 

.. literalinclude:: codes/More-Examples/largest_line.c
    :language: c++
    :linenos:
    :caption: Longest line
    :name: c_largest_line


Store and print values of array
-------------------------------

In :numref:`c_storeArray`, the array size is received from terminal (Line 10), and then values are stored in the array (Lines 14-15). Finally, these values are printed by Lines 20-21. 

.. literalinclude:: codes/More-Examples/storeArray.c
    :language: c++
    :linenos:
    :caption: Store and print value of array
    :name: c_storeArray


Minimum and maximum values of array
-----------------------------------

Lines 23-28 of :numref:`c_small_large_array` find the minimum and maximum values of the array.

.. literalinclude:: codes/More-Examples/small_large_array.c
    :language: c++
    :linenos:
    :caption: Minimum and maximum values of array
    :name: c_small_large_array

Find duplicate elements in array
--------------------------------
    
:numref:`c_find_duplicate` check the duplicates in the array, and prints the location of duplicate elements. 


.. literalinclude:: codes/More-Examples/find_duplicate.c
    :language: c++
    :linenos:
    :caption: Find duplicate elements in array
    :name: c_find_duplicate


Function to sort the numbers in array
-------------------------------------

In :numref:`c_sortArray`, array is sorted by the function 'sortArray' at Line 8. This sorting algorithm is known as 'bubble sort'. 


.. literalinclude:: codes/More-Examples/sortArray.c
    :language: c++
    :linenos:
    :caption: Sort the numbers in array
    :name: c_sortArray

Number conversion
=================

In this section, the numbers are converted into different formats. 

Binary to decimal conversion
----------------------------

:numref:`c_Binary_to_Decimal` converts the binary number to decimal number. 

.. literalinclude:: codes/More-Examples/Binary_to_Decimal.c
    :language: c++
    :linenos:
    :caption: Binary to decimal conversion
    :name: c_Binary_to_Decimal


Decimal to binary conversion
----------------------------

:numref:`c_Decimal_to_binary` converts the decimal number to binary number. 

.. literalinclude:: codes/More-Examples/Decimal_to_binary.c
    :language: c++
    :linenos:
    :caption: Decimal to binary conversion
    :name: c_Decimal_to_binary


Matrices
========


In this section, various matrix operations are implemented using C. 

Size of the matrix
------------------

:numref:`c_elements_of_array` checks the total number of elements in the array. Also, it shows the method to find the number of rows and columns in multidimensional arrays. **Note that arrays are bound for all dimensions except first, therefore it is compulsory to define the second dimension as shown in Line 8 of the listing**.
 

.. literalinclude:: codes/More-Examples/elements_of_array.c
    :language: c++
    :linenos:
    :caption: Size of the matrix
    :name: c_elements_of_array

Dot product
-----------


:numref:`c_dot_product` calculates the dot product of two vectors i.e. :math:`x^T y`, where :math:`x,y \in {R^n}` and :math:`{R^n}` is the short notation for :math:`R^{n \times 1}`. Dot product can be implemented using :numref:`algo_dot_product`. Following are the important points to notice in :numref:`c_dot_product`, 

* For dot product, the size of the vectors should be same. 
* In Line 21, **'const int N'** is used (instead of 'int N'), because the variable 'N' is used for defining the size of array at Lines 22-23, which can not be of variable type. 
* Since, 'N' is constant, therefore it can not be 'passed by reference' during function call; i.e. at Line 27, 'N' is 'passed by value', whereas variable 'result' is 'passed by reference'. 

.. code-block:: shell
    :caption: **Alogrithm** Dot Product : :math:`x,y \in {R^n}`
    :name: algo_dot_product

    for {i = 1:n} do
        result = result + x(i)y(i)
    end for


.. literalinclude:: codes/More-Examples/dot_product.c
    :language: c++
    :linenos:
    :caption: Dot product
    :name: c_dot_product


Matrix vector multiplication
----------------------------

If matrix :math:`A \in {R^{m \times n}}` and vector :math:`x \in {R^n}`, then Matrix-vector multiplication is given by , 


.. math:: 
    :label: eq_matrix_vector_mul    

    {y_i} = \sum\limits_{j = 1}^n {{A_{ij}}{x_j} + {y_i}} ,\;i = 1, \cdots ,m
    

.. code-block:: shell
    :caption: **Algorithm** Matrix-Vector Product : :math:`A \in {R^{m \times n}}` and :math:`x \in {R^n}`
    :name: algo_Matrix_Vector_product

    for i = 1:m do
        for j = 1:n do
            result(i) = result(i) + A(i,j)x(j)
        end for
    end for

:numref:`algo_Matrix_Vector_product` can be used to implement the equation :eq:`eq_matrix_vector_mul`, as shown in :numref:`c_matrix_vector_mul`. **Note that, in the listing, the sizes (Lines 8-9) are defined as global variable**, as we need to pass them in the function prototype and definitions as well; as the arrays are bound for all dimension except first. 

.. note::
    
    Further, we need not to pass the global variable in the function, as shown in :numref:`c_matrix_vector_mul_correct` whose functionality is same as :numref:`c_matrix_vector_mul`; but in :numref:`c_matrix_vector_mul`, global variable is passed to function, just to show the limitations of 'variables' in defining the 'array size'.


.. literalinclude:: codes/More-Examples/matrix_vector_mul.c
    :language: c++
    :linenos:
    :caption: Matrix vector multiplication
    :name: c_matrix_vector_mul

.. literalinclude:: codes/More-Examples/matrix_vector_mul_correct.c
    :language: c++
    :linenos:
    :caption: Matrix vector multiplication (global variables are not passed in function)
    :name: c_matrix_vector_mul_correct


Outer product
-------------

:numref:`algo_outer_product` implements the outer product of two vectors i.e. :math:`x y^T`, where :math:`x \in {R^m}` and :math:`y \in {R^n}` and the result :math:`A \in {R^{m \times n}}`; which is implemented in :numref:`c_outer_product`.


.. code-block:: shell
    :caption: **Algorithm** Outer Product : :math:`x \in {R^m}` and :math:`y \in {R^n}`
    :name: algo_outer_product

    for i = 1:m do
        for j=1:n do
            result(i,j) = result(i,j) + x(i)y(j)
        end for
    end for


.. literalinclude:: codes/More-Examples/outer_product.c
    :language: c++
    :linenos:
    :caption: Outer Product : :math:`x \in {R^m}` and :math:`y \in {R^n}`
    :name: c_outer_product


Matrix-Matrix multiplication
----------------------------

Two matrices :math:`A \in {R^{m \times p}}` and :math:`B \in {R^{p \times n}}`, can be multiplied using :numref:`algo_matrix_matrix_mul`, as shown in :numref:`c_matrix_matrix_mul`. 


.. code-block:: shell
    :caption: **Algorithm** Matrix-Matrix multiplication : :math:`A \in {R^{m \times p}}` and :math:`y \in {R^{p \times n}}`
    :name: algo_matrix_matrix_mul
   
    for i = 1:m do
        for j=1:n do
            for k=1:p do
                result(i,j) = result(i,j) + A(i,k)B(k,j)
            end for
         end for
    end for


.. literalinclude:: codes/More-Examples/matrix_matrix_mul.c
    :language: c++
    :linenos:
    :caption: Matrix-Matrix multiplication : :math:`A \in {R^{m \times p}}` and :math:`y \in {R^{p \times n}}`
    :name: c_matrix_matrix_mul


Upper Triangular Matrices multiplication
----------------------------------------

Two upper triangular matrices :math:`A, B \in {R^{n \times n}}` can be multiplied using :numref:`algo_triangular_matrix_mul`, as shown in :numref:`c_triangular_matrix_mul`.

.. code-block:: shell
    :caption: **Algorithm** Matrix-Matrix multiplication : :math:`A \in {R^{m \times p}}` and :math:`y \in {R^{p \times n}}`
    :name: algo_triangular_matrix_mul

    for i = 1:n do
        for j=i:n do
            for k=i:j do
                result(i,j) = result(i,j) + A(i,k)B(k,j)
            end for
        end for
    end for


.. literalinclude:: codes/More-Examples/triangular_matrix_mul.c
    :language: c++
    :linenos:
    :caption: Triangular Matrix multiplication : :math:`A \in {R^{n \times n}}` and :math:`y \in {R^{n \times n}}`
    :name: c_triangular_matrix_mul


Transpose of matrix
-------------------

Transpose of matrix :math:`A \in {R^{m \times n}}` can be obtained by :numref:`algo_matrix_transpose`, as shown in :numref:`c_matrix_transpose`.


.. code-block:: shell
    :caption: **Algorithm** Transpose of matrix :math:`A \in {R^{m \times n}}`
    :name: algo_matrix_transpose

    for i = 1:m do
        for j=i:n do
            result(j,i) = A(i,j)
        end for
    end for


.. literalinclude:: codes/More-Examples/matrix_transpose.c
    :language: c++
    :linenos:
    :caption: Transpose of matrix :math:`A \in {R^{m \times n}}`
    :name: c_matrix_transpose

Distributions
=============

In this section, various distribution functions are generated, which are used for generating data in mathematical simulations. 

Uniform distribution
--------------------

In Uniform distribution, the random samples are generated with equal density as shown in :numref:`fig_uniform_distribution`. The data for the figure is generated using :numref:`c_uniform_distribution` and saved in the file 'uniform\_data.txt'; then data in the file is plotted using Python as show in :numref:`py_plot_uniform_dist`. 


.. literalinclude:: codes/More-Examples/uniform_distribution.c
    :language: c++
    :linenos:
    :caption: Uniform distribution
    :name: c_uniform_distribution


.. literalinclude:: codes/More-Examples/plot_uniform_dist.py
    :language: python
    :linenos:
    :caption: Plot data generated by :numref:`c_uniform_distribution`
    :name: py_plot_uniform_dist

.. _`fig_uniform_distribution`:

.. figure:: fig/distributions/uniform_distribution.jpeg
    :width: 70%

    Uniform distributions generated by :numref:`c_uniform_distribution`


Gaussian distribution
---------------------

Gaussian distribution can be implemented using 'Box-Muller method'. This method uses 'uniform distribution' for generating the Gaussian distribution, as shown at Lines 35-42 of :numref:`c_gaussian_distribution`. The results of the listing is plotted using :numref:`py_plot_gaussian_dist` and result is shown in :numref:`fig_gaussian_distribution`.

.. _`fig_gaussian_distribution`:

.. figure:: fig/distributions/gaussian_distribution.jpeg
    :width: 70%

    Gaussian distributions generated by :numref:`c_gaussian_distribution`


.. literalinclude:: codes/More-Examples/gaussian_distribution.c
    :language: c++
    :linenos:
    :caption: Gaussian distribution
    :name: c_gaussian_distribution


.. literalinclude:: codes/More-Examples/plot_gaussian_dist.py
    :language: python
    :linenos:
    :caption: Plot data generated by :numref:`c_gaussian_distribution`
    :name: py_plot_gaussian_dist

