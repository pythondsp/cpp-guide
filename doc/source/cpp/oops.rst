.. _`ch_OOPbasic`:

Object Oriented Programming
***************************

.. raw:: latex

    \chapterquote{When a man is, through his desires, confronted with great suffering , he understands their true nature; so when such suffering comes, it should be welcome. Suffering may come in order to eliminate further suffering. Suffering has to come, when it is of use in purging the soul from it's desires. }{Meher Baba}


Introduction
============

Object oriented programming (OOP) increases the re-usability of the code. Also, the codes
become more manageable than non-OOP methods. But, it takes proper planning, and therefore
longer time, to write the codes using OOP method. In this chapter, we will learn following features of OOP along with examples.

* Class and Object
* Data encapsulation (or Data hiding)
* Inheritance
* Polymorphism
* Data abstraction
* Interface (Abstract class)


Class and object
================

A 'class' is user defined template which contains variables, constants and functions etc.; whereas an 'object' is the instance (or variable) of the class. In simple words, a class contains the structure of the code, whereas the object of the class uses that structure for performing various tasks, as shown in this section.

Create class and object
=======================

Class is created using keyword 'class' as shown in Line 6 of :numref:`cpp_classObject1` where the class 'Jungle' is created. As a rule, class name is started with uppercase letter, whereas function name is started with lowercase letter. The class 'Jungle' contains one **public** method i.e. 'welcomeMessage' at Line 10, which prints a message. The 'public' method can be accessed from outside the class. Then an object 'j' of the class Jungle is created at Line 16. Then the public method 'welcomeMessage' is called at Line 17, which prints the message as shown in Line 23. 

.. literalinclude:: codes/Object-Oriented-Programming/classObject1.cpp
    :language: c++
    :linenos:
    :caption: Create class and object
    :name: cpp_classObject1


In :numref:`cpp_classObject2`, the method 'welcomeMessage' has an input parameter i.e. 'name' (see Line 10). Next, a name is read from user using 'getline' at Line 18-19, Then the name is sent as input parameter while calling this the (see Line 22), which is printed by the method as shown in Line 28-28. 

.. literalinclude:: codes/Object-Oriented-Programming/classObject2.cpp
    :language: c++
    :linenos:
    :caption: Method with input parameter
    :name: cpp_classObject2


In :numref:`cpp_classObject2`, the 'name' was provided from the 'main' function, but **did not store in the class**, therefore it can not be used by other methods in the class (if exist). In :numref:`cpp_classObject3`, the name provided by the 'main' function is stored in a **private** variable 'visitorName' (Line 8). Then, two **public** methods, i.e. 'setVisitorName' and 'getVisitorName', are provided to set (Line 35) and get (Line 24) the 'name' of the visitor. 

.. literalinclude:: codes/Object-Oriented-Programming/classObject3.cpp
    :language: c++
    :linenos:
    :caption: Create class and object
    :name: cpp_classObject3

.. note:: 

    Unlike Python, in C++ the class variables should defined as 'private' attributes. This is known as **data hiding**. 

    Data hiding is required in C++ and Java etc. as direct access can be a serious problem here and can not be resolved. In Python, data is not hidden from user and we have various methods to handle all kind of situations as shown in the `Python tutorials <http://pythonguide.readthedocs.io/en/latest/python/oops.html#no-data-hiding-in-python>`_.

Constructor
===========

Whenever, the object of a class is create then all the attributes and methods of that class are attached to it; and the constructor (Lines 13-15) is executed automatically. **Note that, the constructor and the class have the same name**. Here, the constructor contains one variable i.e. 'name' (Line 13). Therefore, when the object 'j' is created at Line 35, the value 'Meher Krishna' will be assigned to parameter 'name' and finally saved in 'visitorName' by the method 'setVisitorName' (Line 18) through constructor (Line 14).

.. literalinclude:: codes/Object-Oriented-Programming/constructorEx.cpp
    :language: c++
    :linenos:
    :caption: Constructor
    :name: cpp_constructorEx

Inheritance
===========

Suppose, we want to write a class 'RateJungle' in which visitor can provide 'rating' based on their visiting-experience. If we write the class from the starting, then we need define attribute 'visitorName' again; which will make the code repetitive and unorganizable, as the visitor entry will be at multiple places and such code is more prone to error. With the help of inheritance, we can avoid such duplication as shown in :numref:`cpp_InheritanceEx`; where class Jungle is inherited at Line 33 by the class 'RateJungle'. Now, when the object 'r' of class 'RateJungle' is created at Line 62 of :numref:`cpp_InheritanceEx`, then this object 'r' will have the access to 'visitorName' as well (which is in the parent class).

.. note:: 

    Two constructors are provided in :numref:`cpp_InheritanceEx`. The first constructor (Line 39) has only one parameter i.e. 'name', whereas the second constructor (Line 44) has two parameters i.e. 'name' and 'rating'. Therefore, if we provide only one parameter (Line 62) then first constructor will be invoked; whereas if we provide two constructor (Line 63), then second constructor will be invoked. 

.. literalinclude:: codes/Object-Oriented-Programming/InheritanceEx.cpp
    :language: c++
    :linenos:
    :caption: Inheritance
    :name: cpp_InheritanceEx


In :numref:`cpp_InheritanceEx2`, an empty constructor is added at Line 54, for setting the value of the object 'p' (Line 74) through Line 83. **Note that, since we added empty constructor in child class, therefore we need to add in parent class as well as shown in Line 18.**

.. literalinclude:: codes/Object-Oriented-Programming/InheritanceEx2.cpp
    :language: c++
    :linenos:
    :caption: Inheritance
    :name: cpp_InheritanceEx2


Polymorphism
============

In OOP, we can use same name for methods and attributes in different classes; the methods or attributes are invoked based on the object type; e.g. in :numref:`cpp_PolymorphismEx`, the method 'scarySound' is used for class 'Animal' and 'Bird' at Lines 8 and 15 respectively. Then object of these classes are created at Line 26-27. Finally, method 'scarySound' is invoked at Lines 29-30; here Line 29 is the object of class Animal, therefore method of that class is invoked and corresponding message is printed. Similarly, Line 30 invokes the 'scaryMethod' of class Bird and corresponding line is printed.

.. literalinclude:: codes/Object-Oriented-Programming/PolymorphismEx.cpp
    :language: c++
    :linenos:
    :caption: Polymorphism
    :name: cpp_PolymorphismEx


Abstract class
==============

Abstract classes are the classes which contains one or more abstract method; and abstract methods are the methods which does not contain any implementation, but the child-class need to implement these methods otherwise error will be reported. In this way, we can force the child-class to implement certain methods in it. We can define, abstract classes and abstract method using keyword 'virtual void', as shown in Line 27 of :numref:`cpp_abstractClassEx`. Since, 'scarySound' is defined as abstract method at Line 27, therefore it is compulsory to implement it in all the subclasses.

.. note::

    Look at the class 'Insect' in :numref:`cpp_PolymorphismEx`, where 'scarySound' was not defined but code was running correctly; but now the 'scarySound' is abstract method, therefore it is compulsory to implement it, as done in Line 16 of :numref:`cpp_abstractClassEx`.

.. literalinclude:: codes/Object-Oriented-Programming/abstractClassEx.cpp
    :language: c++
    :linenos:
    :caption: Abstract class
    :name: cpp_abstractClassEx

